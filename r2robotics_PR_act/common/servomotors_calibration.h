//
// Created by ksyy on 23/01/17.
//

#ifndef ROS_SERVOMOTORS_CALIBRATION_H
#define ROS_SERVOMOTORS_CALIBRATION_H

#include "common_operators.h"


static constexpr int servo1CalibrationIndices[ 21 ]{ 0, 94, 140, 187, 233, 279, 327, 374, 419, 467, 513, 558, 605, 651,
                                                     698, 746, 790, 838, 884, 929, 1023 };
static constexpr double servo1CalibrationValues[ 21 ]{ -2.92183499, -2.36735653, -2.09601601, -1.82914935, -1.56302154,
                                                        -1.29435719, -1.01725709, -.75112346, -.51268240, -.24149314,
                                                        .00954920, .25377894, .52120702, .78589105, 1.04236233, 1.30257109,
                                                        1.54772242, 1.81475779, 2.07559725, 2.33861837, 2.88244714 }; 

static constexpr int servo2CalibrationIndices[ 21 ]{ 0, 96, 141, 187, 235, 280, 327, 373, 420, 466, 513, 557, 605, 651,
                                                     698, 745, 791, 838, 885, 931, 1023 };
static constexpr double servo2CalibrationValues[ 21 ]{ -2.91860330, -2.35926018, -2.09706809, -1.83038178, -1.55720866,
                                                        -1.29980169, -1.02530503, -.74666321, -.48307486, -.23017666,
                                                        .02312880, .25986816, .52843070, .78460140, 1.04917877, 1.30754568,
                                                        1.54438686, 1.79929220, 2.06379685, 2.32760336, 2.83284391 }; 

static constexpr int servo3CalibrationIndices[ 21 ]{ 0, 95, 140, 187, 234, 280, 327, 374, 420, 466, 513, 559, 605, 651,
                                                     698, 745, 791, 838, 884, 931, 1023 };
static constexpr double servo3CalibrationValues[ 21 ]{ -2.91984532, -2.35315630, -2.08472466, -1.81703479, -1.54925765,
                                                        -1.29343601, -1.02408807, -.75600550, -.50066383, -.24977275, .00583072,
                                                        .25462740, .50098063, .76984860, 1.03029535, 1.29623990, 1.55385050,
                                                        1.81621712, 2.07736201, 2.35870908, 2.92061726 }; 

constexpr double interpolate( const int indices[ 21 ], const double values[ 21 ], int index )
{
	if( index <= indices[ 0 ] )
		return values[ 0 ];
	
	for( int i{ 1 }; i < 21 ; ++i )
	{
		if( index <= indices[ i ] )
			return values[ i ] + ( static_cast< double >( index - indices[ i ] ) *
					               	   ( values[ i ] - values[ i - 1  ] ) /
			                           static_cast< double >( indices[ i ] - indices[ i - 1 ] ) );
	}
	
	return values[ 20 ];
}

constexpr double servoValuesInt( const int servo, const int index )
{
	if( servo == 0 )
		return interpolate( servo1CalibrationIndices, servo1CalibrationValues, index );
	else if( servo == 1 )
		return interpolate( servo2CalibrationIndices, servo2CalibrationValues, index );
	else
		return interpolate( servo3CalibrationIndices, servo3CalibrationValues, index );
}

template< int Size >
struct ArrayDouble
{
	double values[ Size ];
};

// Magic with indices !
template< unsigned... Is > struct seq{};
template< unsigned N, unsigned... Is >
struct gen_seq : gen_seq< N - 1, N - 1, Is... >{};
template< unsigned... Is >
struct gen_seq< 0, Is... > : seq< Is... >{};

template< unsigned... Ind >
constexpr ArrayDouble< sizeof...( Ind ) > makeInterpolationArray( const int indices[ 21 ], const double values[ 21 ], seq< Ind... > )
{
	return {{ interpolate( indices, values, Ind )... }};
}

template< unsigned Size >
constexpr ArrayDouble< Size > makeInterpolationArray( const int indices[ 21 ], const double values[ 21 ] )
{
	return makeInterpolationArray( indices, values, gen_seq< Size >{} );
}

#endif //ROS_SERVOMOTORS_CALIBRATION_H
