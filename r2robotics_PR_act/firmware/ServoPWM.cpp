//
// Created by manon on 5/22/17.
//

#include "ServoPWM.h"

void ServoPWM::init( int pin ) {
    delay(10);
    _servo.attach(pin);
    true_value = 0;
    false_value = 0;
    coeff = 1;
    delay(10);
}

void ServoPWM::init( int pin, float _coeff ) {
    delay(10);
    _servo.attach(pin);
    true_value = 0;
    false_value = 0;
    coeff = _coeff;
    delay(10);
}

void ServoPWM::init( int pin, int _false_value, int _true_value ) {
    delay(10);
    _servo.attach(pin);
    true_value = _true_value;
    false_value = _false_value;
    coeff = 1;
    delay(10);
}

void ServoPWM::init( int pin, int _false_value, int _true_value, float _coeff ) {
    delay(10);
    _servo.attach(pin);
    true_value = _true_value;
    false_value = _false_value;
    coeff = _coeff;
    delay(10);
}

void ServoPWM::servoPWM(int pwm) {
   delay(100);
    _servo.writeMicroseconds(pwm);
   delay(100);
}

void ServoPWM::servoDegrees(int value) {
   delay(100);
    _servo.write(coeff * value);
   delay(100);
}

void ServoPWM::servoTrueFalse(bool value) {
   delay(100);
    if( value )
        _servo.write(coeff * true_value);
    else
        _servo.write(coeff * false_value);
   delay(100);
}