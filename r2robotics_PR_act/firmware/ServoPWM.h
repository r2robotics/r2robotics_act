//
// Created by manon on 5/22/17.
//

#ifndef CATKIN_WS_SERVOSPWM_H
#define CATKIN_WS_SERVOSPWM_H

#include <Arduino.h>
#include <Servo.h>

class ServoPWM{
public:
    Servo _servo;
    int false_value;
    int true_value;
    float coeff;

    void init( int pin );
    void init( int pin, float _coeff );
    void init( int pin, int _low_value, int _high_value );
    void init( int pin, int _false_value, int _true_value, float _coeff );
    void servoPWM( int pwm );
    void servoDegrees(int pwm);
    void servoTrueFalse( bool open );
};

#endif //CATKIN_WS_SERVOSPWM_H
