//
// Created by ksyy on 08/05/18.
//

#ifndef ROS_HIGH_LEVEL_ACT_H
#define ROS_HIGH_LEVEL_ACT_H

#include <ros/ros.h>

#include <vector>
#include <array>

#include "r2robotics_srvs/move_direction.h"


class HighLevelAct
{
public:
	HighLevelAct();

	void registerServices( ros::NodeHandle *nh, std::string prefix = "" );

	bool moveDirection( r2robotics_srvs::move_direction::Request  &req,
			   		    r2robotics_srvs::move_direction::Response &answer );

private:

	std::vector< ros::ServiceServer > _servers;

	ros::NodeHandle* _nodeHandle;
	std::string _prefix;
};



#endif //ROS_HIGH_LEVEL_ACT_H
